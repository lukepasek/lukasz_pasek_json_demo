package lp.demo.json.validator;

import lp.demo.json.validator.exception.ValidationException;

public interface ValueValidator {
	public void validate(Object value) throws ValidationException;

	public String getName();
}
