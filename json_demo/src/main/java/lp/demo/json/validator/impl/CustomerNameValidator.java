package lp.demo.json.validator.impl;

import java.util.HashSet;

import lp.demo.json.validator.ValueValidator;
import lp.demo.json.validator.exception.ValidationException;

public class CustomerNameValidator implements ValueValidator {
	
	private HashSet<String> validNames = new HashSet<>();
	
	public CustomerNameValidator() {
		validNames.add("PLUTO1");
		validNames.add("PLUTO2");
	}

	@Override
	public void validate(Object value) throws ValidationException {
		if (validNames==null || !validNames.contains(value)) {
			throw new ValidationException("Invalid customer name: \""+value+"\"");
		}

	}

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

}
