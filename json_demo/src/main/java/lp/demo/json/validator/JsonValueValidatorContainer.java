package lp.demo.json.validator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class JsonValueValidatorContainer {
	private ArrayList<JsonValueValidator> validators = new ArrayList<>();
	
	public void addValidatorHandler(JsonValueValidator validator) {
		validators.add(validator);
	}
	
	public List<JsonValueValidator> getValidators() {
		return validators;
	}
}
