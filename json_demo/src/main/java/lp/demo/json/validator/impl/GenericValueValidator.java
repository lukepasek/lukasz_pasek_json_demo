package lp.demo.json.validator.impl;

import lp.demo.json.util.JsonWalker;
import lp.demo.json.validator.JsonValueValidatorComponent;
import lp.demo.json.validator.exception.ValidationException;
import net.sf.json.JSONObject;

public class GenericValueValidator extends JsonValueValidatorComponent {
	
	private String name = "GenericValueValidator";
	private String validValue;
	private String invalidValueMessage;
	private String jsonParameterName;
	private String[] jsonParameterPath;
	
	public void setJsonParameterName(String jsonParameterName) {
		this.jsonParameterName = jsonParameterName;
		this.jsonParameterPath = jsonParameterName.split("\\.");
	}
	
	public void setValidatorName(String name) {
		this.name = name;
	}

	public void setValidValue(String validValue) {
		this.validValue = validValue;
	}

	public void setInvalidValueMessage(String invalidValueMessage) {
		this.invalidValueMessage = invalidValueMessage;
	}

	@Override
	protected void validatorInit() {
	}

	@Override
	public void validate(JSONObject root) throws ValidationException {
		validate(root, jsonParameterPath);
	}

	@Override
	public void validate(JSONObject root, String namePath)
			throws ValidationException {
		validate(root, namePath.split("\\."));

	}
	
	public void validate(JSONObject root, String[] namePath)
			throws ValidationException {
		String value = JsonWalker.getAsString(root, namePath);
		if (value==null || !validValue.equals(String.valueOf(value))) {
			throw new ValidationException(invalidValueMessage+": \""+value+"\"");
		}

	}

	@Override
	public String getName() {
		return name;
	}

}
