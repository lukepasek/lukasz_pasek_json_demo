package lp.demo.json.validator;

import lp.demo.json.util.JsonWalker;
import lp.demo.json.validator.exception.ValidationException;
import net.sf.json.JSONObject;

public class JsonValueValidatorWrapper extends JsonValueValidatorComponent {
	private String[] jsonParameterPath;
	private ValueValidator validator;

	private String jsonParameterName;
	private String validatorClassName;
	private String valueTypeName;

	public void setJsonParameterName(String jsonParameterName) {
		this.jsonParameterName = jsonParameterName;
	}

	public void setValidatorClassName(String validatorClassName) {
		this.validatorClassName = validatorClassName;
	}

	public void setValueTypeName(String valueTypeName) {
		this.valueTypeName = valueTypeName;
	}

	public void validatorInit() {
		jsonParameterPath = jsonParameterName.split("\\.");
		try {
			Class<ValueValidator> validatorImpl = (Class<ValueValidator>) ValueValidator.class.forName(validatorClassName);
			validator = validatorImpl.newInstance();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void validate(JSONObject root, String[] parameterPath) throws ValidationException {
		if (validator instanceof JsonValueValidator) {
			((JsonValueValidator)validator).validate(root, jsonParameterName);
		} else {
			Object value = JsonWalker.get(root, jsonParameterName);
			
			if (valueTypeName!=null) {
				// TODO add specific type convertion/extraction
			} else { // default string parameter type
				if (value instanceof JSONObject)
					validator.validate(((JSONObject)value).toString());
				validator.validate(String.valueOf(value));
			}
		}
	}

	public void validate(JSONObject root) throws ValidationException {
		validate(root, jsonParameterName);
	}
	
	public void validate(JSONObject root, String parameterPath) throws ValidationException {
		validate(root, parameterPath.split("\\."));
	}

	@Override
	public String getName() {
		return validator.getName();
	}

}
