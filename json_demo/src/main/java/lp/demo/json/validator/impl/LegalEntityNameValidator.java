package lp.demo.json.validator.impl;

import lp.demo.json.validator.ValueValidator;
import lp.demo.json.validator.exception.ValidationException;

public class LegalEntityNameValidator implements ValueValidator {
	
	private static String VALID_ENTITY_NAME = "CS Zurich";

	@Override
	public void validate(Object value) throws ValidationException {
		if (value==null || !VALID_ENTITY_NAME.equals(String.valueOf(value))) {
			throw new ValidationException("Invalid legal entity name: \""+value+"\"");
		}

	}

	@Override
	public String getName() {
		return "LegalEntityNameValidator";
	}

}
