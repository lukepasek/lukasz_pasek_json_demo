package lp.demo.json.validator;

import lp.demo.json.util.JsonWalker;
import lp.demo.json.validator.exception.ValidationException;
import net.sf.json.JSONObject;

public class JsonValueValidationCondition {

	enum Test {
		EQ,
		NE,
		//		LT,
		//		GT
	}

	private Test test = Test.EQ;
	private String[] jsonParameterPath;
	private String testValue;

	public void setJsonParameterName(String jsonParameterName) {
		this.jsonParameterPath = jsonParameterName.split("\\.");
	}

	public void setTest(String test) {
		this.test = Test.valueOf(test.toUpperCase());
	}
	
	public void setTestValue(String value) {
		this.testValue = value;
	}

	public boolean test(JSONObject root) throws ValidationException {
		String value = JsonWalker.getAsString(root, jsonParameterPath);

		if (value==null) return false;
		
		switch (test) {
		case EQ: return testValue.equals(value);
		case NE: return !testValue.equals(value);
		}
		return false;
	}

}
