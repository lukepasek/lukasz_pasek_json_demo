package lp.demo.json.validator;

import lp.demo.json.validator.exception.ValidationException;
import net.sf.json.JSONObject;

public interface JsonValueValidator {
	public String getName();
	public boolean checkValidateConditions(JSONObject value) throws ValidationException;
	public void validate(JSONObject value) throws ValidationException;
	public void validate(JSONObject value, String namePath) throws ValidationException;
}
