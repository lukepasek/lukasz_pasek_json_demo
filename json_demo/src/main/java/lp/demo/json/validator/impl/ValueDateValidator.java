package lp.demo.json.validator.impl;

import java.util.Date;

import lp.demo.json.validator.ValueValidator;
import lp.demo.json.validator.exception.ValidationException;

public class ValueDateValidator implements ValueValidator {

	@Override
	public void validate(Object value) throws ValidationException {
		if (value instanceof Long) {
			
		} else if (value instanceof Date) {
			
		} else if (value instanceof String) {
			// TODO parse string date
		} else {
			throw new ValidationException("Invalid date parameter format!");
		}
	}

	@Override
	public String getName() {
		return "ValueDateValidator";
	}

}
