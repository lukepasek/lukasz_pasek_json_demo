package lp.demo.json.validator.impl;

import java.util.Currency;

import lp.demo.json.util.JsonWalker;
import lp.demo.json.validator.JsonValueValidatorComponent;
import lp.demo.json.validator.exception.ValidationException;
import net.sf.json.JSONObject;

public class CurrencyValidator extends JsonValueValidatorComponent {

	private String[] jsonParameterPath;

	public void setJsonParameterName(String jsonParameterName) {
		this.jsonParameterPath = jsonParameterName.split("\\.");
	}

	@Override
	protected void validatorInit() {
	}

	@Override
	public String getName() {
		return "CurrencyValidator";
	}

	@Override
	public void validate(JSONObject root) throws ValidationException {
		validate(root, jsonParameterPath);
	}

	@Override
	public void validate(JSONObject root, String namePath)
			throws ValidationException {
		validate(root, namePath.split("\\."));
	}

	public void validate(JSONObject root, String[] namePath)
			throws ValidationException {
		String currency = JsonWalker.getAsString(root, namePath);
		try {
			Currency.getInstance(currency);
		} catch (IllegalArgumentException ie) {
			throw new ValidationException("Invalid currency name: \""+currency+"\" in currency parameter: \""+String.join(".", jsonParameterPath)+"\"");

		}
	}

}
