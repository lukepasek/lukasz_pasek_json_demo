package lp.demo.json.validator.impl;

import java.util.HashSet;
import java.util.List;

import lp.demo.json.util.JsonWalker;
import lp.demo.json.validator.JsonValueValidatorComponent;
import lp.demo.json.validator.exception.ValidationException;
import net.sf.json.JSONObject;

public class OptionsStyleValidator extends JsonValueValidatorComponent {

	private static final String[] VALID_OPTION_STYLES = { "AMERICAN", "EUROPEAN" };

	private String[] jsonParameterPath;

	private HashSet<String> validOptionStyles = new HashSet<String>();

	@Override
	protected void validatorInit() {
		if (validOptionStyles.isEmpty())
			for (String style: VALID_OPTION_STYLES) {
				validOptionStyles.add(style);
			}
	}
	
	public void setOptionStyleNames(List<String> optionStyleNames) {
		this.validOptionStyles.addAll(optionStyleNames);
	}

	public void setJsonParameterName(String jsonParameterName) {
		this.jsonParameterPath = jsonParameterName.split("\\.");
	}

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

	@Override
	public void validate(JSONObject root, String namePath)
			throws ValidationException {
		throw new ValidationException("Validation method not supported!");

	}

	@Override
	public void validate(JSONObject root)
			throws ValidationException {
		String style = JsonWalker.getAsString(root, jsonParameterPath);
		if (!validOptionStyles.contains(style)) {
			throw new ValidationException("Invalid option style: "+style);
		}

	}

}
