package lp.demo.json.validator;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import lp.demo.json.validator.exception.ValidationException;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;

public abstract class JsonValueValidatorComponent implements JsonValueValidator {
	
	ArrayList<JsonValueValidationCondition> validateConditions;
	
	public void setValidateConditions(List<JsonValueValidationCondition> conditions) {
		if(validateConditions==null) validateConditions = new ArrayList<JsonValueValidationCondition>();
		validateConditions.addAll(conditions);
	}
	
	@Autowired
	private JsonValueValidatorContainer container;
	
	@PostConstruct
	public void init() {
		validatorInit();
		container.addValidatorHandler(this);
	}
	
	protected abstract void validatorInit();
	
	@Override
	public boolean checkValidateConditions(JSONObject root) throws ValidationException {
		if (validateConditions==null || validateConditions.isEmpty()) return true;
		for (JsonValueValidationCondition cond: validateConditions) {
			if (cond.test(root)) return true;
		}
		return false;
	}
}
