package lp.demo.json;

import java.util.ArrayList;
import java.util.HashMap;

import lp.demo.json.validator.JsonValueValidatorContainer;
import lp.demo.json.validator.JsonValueValidator;
import lp.demo.json.validator.exception.ValidationException;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@EnableAutoConfiguration
@ImportResource("classpath:spring-trade-validator-config.xml")
public class TradeValidatorService {
	
	@Autowired
	JsonValueValidatorContainer validatorContainer;

	@RequestMapping(value="/", method=RequestMethod.POST, produces={"application/json"}, consumes="application/json")
	@ResponseBody
	ResponseEntity<String> addTradeJson(@RequestBody String jsonInput) {
		
		HashMap<Integer, ArrayList<String>> validateExceptions = null;
		int tradeCount = 0;
		
		jsonInput = jsonInput.trim();
		
		if (jsonInput.startsWith("{")) {
			JSONObject trade = JSONObject.fromObject(jsonInput);
			
			ArrayList<String> tradeValidateExceptions = validateTrade(trade);
			if (tradeValidateExceptions!=null) {
				if (validateExceptions==null) validateExceptions = new HashMap<Integer, ArrayList<String>>();
				validateExceptions.put(tradeCount, tradeValidateExceptions);
			}
		
			tradeCount++;
		} else if (jsonInput.startsWith("[")) {
			JSONArray trades = JSONArray.fromObject(jsonInput);
		
			for (Object trade: trades) {
				if (trade instanceof JSONObject) {
					ArrayList<String> tradeValidateExceptions = validateTrade((JSONObject)trade);
					if (tradeValidateExceptions!=null) {
						if (validateExceptions==null) validateExceptions = new HashMap<Integer, ArrayList<String>>();
						validateExceptions.put(tradeCount, tradeValidateExceptions);
					}
					tradeCount++;
				}
			}
		}
		
		if (validateExceptions == null || validateExceptions.isEmpty()) {
			JSONObject result = new JSONObject();
			result.accumulate("tradeAccepted", true);
			result.accumulate("tradeCount", tradeCount);
			return	ResponseEntity.ok(result.toString());
		} else {
			JSONObject result = new JSONObject();
			result.accumulate("tradeAccepted", false);
			result.accumulate("tradeCount", tradeCount);
			result.accumulate("tradeInvalidCount", validateExceptions.size());
			JSONArray errors = new JSONArray();
			for (Integer id: validateExceptions.keySet()) {
				JSONObject error = new JSONObject();
				error.accumulate("tradeIndex", id);
				JSONArray tradeErrors = new JSONArray();
				ArrayList<String> tradeExceptionsMessages = validateExceptions.get(id);
				
				for (String tex: tradeExceptionsMessages) {
					tradeErrors.add(tex);
				}
				error.accumulate("validationErrors", tradeErrors);
				errors.add(error);
			}
			result.accumulate("tradeValidationErrors", errors);
			return	ResponseEntity.ok(result.toString());
		}
//		try {
//			return	ResponseEntity.ok(JsonSoapProxyService.call(serviceName, operationName, jsonInput, null));
//		} catch (JsonSoapProxyException e) {
//			return ResponseEntity.status(400).body(e.getJsonError());
//		}
	}


	private ArrayList<String> validateTrade(JSONObject trade) {
		ArrayList<String> tradeValidateExceptions = null;
		for (JsonValueValidator v: validatorContainer.getValidators()) {
			try {
				if (v.checkValidateConditions(trade))
					v.validate(trade);
			} catch (ValidationException e) {
				if (tradeValidateExceptions==null) tradeValidateExceptions = new ArrayList<String>();
				tradeValidateExceptions.add(v.getName()+": "+e.getMessage());
			}
		}
		return tradeValidateExceptions;
	}


	public static void main(String[] args) throws Exception {
		SpringApplication.run(TradeValidatorService.class, args);
	}
}