package lp.demo.json.util;

import lp.demo.json.validator.exception.ValidationException;
import net.sf.json.JSONObject;

public class JsonWalker {
	public static Object get(JSONObject root, String parameterName) throws ValidationException {
		return get(root, parameterName.split("\\."));
	}
	
	public static Object get(JSONObject root, String[] parameterPath) throws ValidationException {
		Object value = null;
		for (String pName: parameterPath) {
			if (root.has(pName)) {
				value = root.get(pName);
				if (value==null) {
					throw new ValidationException("Parameter value is null in input data: '"+String.join(".", parameterPath)+"'");
				}
				if (value instanceof JSONObject) {
					root = (JSONObject) value;
				}
			} else {
				throw new ValidationException("Missing parameter in input data: '"+String.join(".", parameterPath)+"'");
			}
		}
		return value;
	}

	public static String getAsString(JSONObject root, String[] jsonParameterPath) throws ValidationException {
		Object value = get(root, jsonParameterPath);
		if (value!=null) {
			return String.valueOf(value);
		}
		return null;
	}
}
